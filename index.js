const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
  fs.readFile('index.html', (error, content) => {
    res.end(content.toString());
  });
});

server.listen(8080, () => {
  console.log('Listening on port 8080.');
});
