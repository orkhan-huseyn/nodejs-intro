
const number = parseInt(process.argv[2]);

function isPrime(n) {
  for (let i = 2; i < n; i++) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

if (isPrime(number)) {
  console.log('Sadədir!');
} else {
  console.log('Mürəkkəbdir!');
}
